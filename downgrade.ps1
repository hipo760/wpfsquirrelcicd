param (
    $HAS_DEPLOYED = "false",
    $HAS_MERGED = "false"
    )

Write-Output "HAS_DEPLOYED" $HAS_DEPLOYED
Write-Output "HAS_MERGED" $HAS_MERGED

if ($HAS_DEPLOYED -eq "true" -and $HAS_MERGED -eq "false") {
    Write-Output "Start downgrading...." 
    git branch --show-current
    Write-Output "Downgra done" 
    Exit 0    
}

Write-Output "Downgrade job is forbidden." 
Exit 1